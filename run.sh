#! /bin/sh

docker run -d --name graphite --restart=always \
	-p 127.0.0.1:1080:80 \
	-p 172.27.27.1:2003-2004:2003-2004 \
	-p 172.27.27.1:2023-2024:2023-2024 \
	-p 172.27.27.1:8125:8125/udp \
	-p 127.0.0.1:8126:8126 \
	-v /opt/graphite/conf:/opt/graphite/conf \
	-v /opt/graphite/data:/opt/graphite/storage \
	-v /opt/statsd:/opt/statsd \
	anight/graphite-statsd
